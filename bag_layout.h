#ifndef BAG_LAYOUT_H
#define BAG_LAYOUT_H
#include "container.h"
#include "counter.h"
using namespace sjp;
namespace mhlzol004{
class bag_layout:public container, public counter<bag_layout>{
public:
    int h;int w;
    bag_layout(int w,int h);
    virtual ~bag_layout();
    virtual void render(std::ostream &stream, unsigned int depth);
};
}
#endif // BAG_LAYOUT_H
