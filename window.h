#ifndef WINDOW_H
#define WINDOW_H
#include<iostream>
#include "container.h"
#include<counter.h>
using namespace sjp;
namespace mhlzol004{
class window: public container, public counter<window>{
public:
    char *title;
    window(char *some_title);
    virtual ~window();
    virtual void render(std::ostream &stream, unsigned int depth);
};
}
#endif // WINDOW_H
