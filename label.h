#ifndef LABEL_H
#define LABEL_H
#include "widget.h"
#include "counter.h"
using namespace sjp;
namespace mhlzol004{
class label: public widget, public counter<label>{
public:
    char *text;
    label(char *text);
    label(label &otherlabel);
    virtual ~label();
    void operator=(label &otherlabel);
    virtual void render(std::ostream &stream, unsigned int depth);
};
}
#endif // LABEL_H
