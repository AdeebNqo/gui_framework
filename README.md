Assignment 3, CSC3022H
======================
Zola Mahlaza, MHLZOL004
=======================

The makefile has been generated using qmake, it was a QtCreator project.

To compile:

            make
To run:

            make run
To clean up binary files:

            make clean

List of files:
--------------
    0. container.cpp (container.h)
    1. bag_layout.cpp (bag_layout.h)
    2. button.cpp (button.h)
    3. menu.cpp (menu.h)
    4. gui_framework.pro (needed by makefile)
    5. label.cpp (label.h)
    6. window.cpp (window.h)
    7. menu_item.cpp (menu_item.h)
    8. gui.cpp (gui.h)
    9. main.cpp
    10. widget.h
    11. Makefile
    12. counter.h
    13. ReadMe.in

All the files represent the class specified in the assignment document.

Output
-------
        ./gui_framework
        --------------------RENDERING---------------------------
        Window "hello"
            Window "hello"
                Label "tutor"
            Menu "hello" "tutor"
                Menu Item "tutor" "hello"
            BagLayout 300x300
                Menu "tutor" "hello"
                Button "tutor" Button is pressed
                Button "Cancel" Button is not pressed

Important notice
----------------

I was unsure of certain parts of the assignment spec. Especialy the ff:

    Menu should be rendered as: Menu "File" "Ctrl F"
    
I was unsure whether thi meant that everytime you call render() on a Menu object, I was supposed to output the given line. I then decided to make the constructor take two c-strings, say A and B. Then when you render a Menu objects it displays:

        Menu "A" "B"

Instead of a fixed line. I also has the same confusion about most of the render lines/strings.
