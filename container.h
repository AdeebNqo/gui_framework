#ifndef CONTAINER_H
#define CONTAINER_H
#include "widget.h"
#include<vector>
namespace mhlzol004{
class container: public widget{
public:
    std::vector<widget*> children;
    container();
    container(container &other_container);
    virtual ~container();
    void operator=(container &other_container);
    virtual void add_child(widget* some_widget);
    virtual void render(std::ostream &stream, unsigned int depth);
};
}
#endif // CONTAINER_H
