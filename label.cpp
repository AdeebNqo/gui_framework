/*
Copyright 2013, Zola Mahlaza
http://www.gnu.org/licenses/gpl-2.0.txt

label class
assignment 2 of csc3022
*/
#include "label.h"
#include <cstring>
using namespace mhlzol004;
label::label(char *text_label){
    using namespace std;
    int len = (unsigned)strlen(text_label);
    text = new char[len];
    strcpy(text, text_label);
}
label::~label(){
    delete []text;
}
label::label(label &other_label){
    int len = strlen(other_label.text);
    text = new char[len];
    strcpy(text, other_label.text);
}
void label::render(std::ostream &stream, unsigned int depth){
    std::string tmp(text);
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    stream<<"Label \""<<tmp.substr(0,tmp.find_first_of('\n'))<<"\""<<std::endl;
}
