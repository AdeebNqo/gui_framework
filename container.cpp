/*
Copyright 2013, Zola Mahlaza
http://www.gnu.org/licenses/gpl-2.0.txt

container class
assignment 2 of csc3022
*/
#include "container.h"
using namespace mhlzol004;
container::container(){
    children = std::vector<widget*>();
}
container::container(container &other_container){
    for (std::vector<widget*>::iterator it = other_container.children.begin(); it!=other_container.children.end();++it){
        children.push_back(*it);
    }
}
container::~container(){
    for (std::vector<widget*>::iterator it = children.begin(); it!=children.end(); ++it){
        delete *it;
    }
}
void container::operator =(container &other_container){
    children = other_container.children;
}
void container::add_child(widget* some_widget){
    this->children.push_back(some_widget);
}
void container::render(std::ostream &stream, unsigned int depth){
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    stream<<"Container"<<std::endl;
    for (std::vector<widget*>::iterator it = children.begin(); it!=children.end(); ++it){
        (*it)->render(std::cout,depth+1);
    }
}
