/*
Copyright 2013, Zola Mahlaza
http://www.gnu.org/licenses/gpl-2.0.txt

gui class
assignment 2 of csc3022
*/
#include "gui.h"
#include<iostream>
using namespace mhlzol004;
gui::gui(){
}
gui::~gui(){
    delete ancestor;
}
gui::gui(gui& some_gui){
    if (this == &some_gui){
        return;
    }
    *ancestor = *some_gui.ancestor;
    for (std::vector<widget*>::iterator curr_pos = some_gui.children.begin(); curr_pos!=some_gui.children.end();++curr_pos){
        this->children.push_back(*curr_pos);
    }
}
void gui::operator =(gui& some_other_gui){
    if (this == &some_other_gui){
        return;
    }
    this->ancestor = some_other_gui.ancestor;
    this->children = some_other_gui.children;
}
void gui::render(std::ostream &stream){
    this->ancestor->render(stream, 0);
}
