TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += \
    gui.cpp \
    main.cpp \
    label.cpp \
    button.cpp \
    menu_item.cpp \
    container.cpp \
    window.cpp \
    bag_layout.cpp \
    menu.cpp

HEADERS += \
    widget.h \
    gui.h \
    label.h \
    button.h \
    menu_item.h \
    container.h \
    window.h \
    bag_layout.h \
    menu.h \
    counter.h
