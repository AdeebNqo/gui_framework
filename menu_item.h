#ifndef MENU_ITEM_H
#define MENU_ITEM_H
#include<iostream>
#include "widget.h"
#include "counter.h"
using namespace sjp;
namespace mhlzol004{
class menu_item: public widget, public counter<menu_item>{
public:
    char *text;
    char *key_combination;
    menu_item(char *text,char * kc);
    menu_item(menu_item &other_menu_item);
    virtual ~menu_item();
    virtual void render(std::ostream &stream, unsigned int depth);
};
}
#endif // MENU_ITEM_H
