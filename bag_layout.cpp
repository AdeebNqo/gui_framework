/*
Copyright 2013, Zola Mahlaza
http://www.gnu.org/licenses/gpl-2.0.txt

bag_layout class
assignment 2 of csc3022
*/
#include "bag_layout.h"
using namespace mhlzol004;
bag_layout::bag_layout(int width, int height){
    w = width;
    h = height;
}
bag_layout::~bag_layout(){}
void bag_layout::render(std::ostream &stream, unsigned int depth){
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    stream << "BagLayout "<<w<<"x"<<h<<std::endl;
    for (std::vector<widget*>::iterator it = children.begin(); it!=children.end(); ++it){
        (*it)->render(std::cout,depth+1);
    }
}
