#ifndef MAIN_CPP
#define MAIN_CPP
#include "gui.h"
#include "menu_item.h"
#include "window.h"
#include "menu.h"
#include "button.h"
#include "label.h"
#include "bag_layout.h"
using namespace mhlzol004;
void print_counts();
void process_gui();
int main(void){
    process_gui();
    print_counts();
    return 0;
}
void process_gui(){
    std::cout<<"--------------------RENDERING---------------------------"<<std::endl;
    //strings that are goin goin' to be used in the test cases
    char tmp[6] = {'h','e','l','l','o','\n'};
    char tmp1[6] = {'t','u','t','o','r','\n'};

    gui ui;
    window *big_win = new window(tmp); //window that's gonna contain both container
    ui.ancestor = big_win;
        //first container
        window *win = new window(tmp);
        big_win->add_child(win);
        //label
        label *lab = new label(tmp1);
        menu *men = new menu(tmp, tmp1);
        menu_item *item = new menu_item(tmp1,tmp);
        men->add_child(item);
        win->add_child(lab);
        win->add_child(men);
        //second container
        bag_layout *bag = new bag_layout(300,300);
        big_win->add_child(bag);
        menu *sec_menu = new menu(tmp1,tmp);
        bag->add_child(sec_menu);
        button *but = new button(tmp1);
        bag->add_child(but);
     ui.render(std::cout);
}

//method for getting the count for all objects
void print_counts(){
    std::cout<<"----------------------Count-----------------------------"<<std::endl;
    bag_layout::print_counts(std::cout, "bag_layout");
    //window::print_counts(std::cout,"window");
    label::print_counts(std::cout,"label");
    menu::print_counts(std::cout, "menu");
    menu_item::print_counts(std::cout,"menu_item");
    window::print_counts(std::cout,"window");
    button::print_counts(std::cout,"button");
}
#endif // MAIN_CPP
