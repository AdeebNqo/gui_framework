/*
Copyright 2013, Zola Mahlaza
http://www.gnu.org/licenses/gpl-2.0.txt

button class
assignment 2 of csc3022
*/
#include "button.h"
#include<cstring>
using namespace mhlzol004;
button::button(char *string){
    int len = (unsigned)strlen(string);
    text = new char[len];
    strcpy(text, string);
}
button::button(button &other_button){
    int len = (unsigned)strlen(other_button.text);
    text = new char[len];
    strcpy(text, other_button.text);
}
button::~button(){
    delete []text;
}
void button::render(std::ostream &stream, unsigned int depth){
    std::string tmp(text);
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    stream<<"Button \""<<tmp.substr(0,tmp.find_first_of('\n'))<<"\" Button is pressed"<<std::endl;
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    stream<<"Button \"Cancel\" Button is not pressed"<<std::endl;
}
