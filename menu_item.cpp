/*
Copyright 2013, Zola Mahlaza
http://www.gnu.org/licenses/gpl-2.0.txt

menu_item class
assignment 2 of csc3022
*/
#include "menu_item.h"
#include<cstring>
using namespace mhlzol004;
menu_item::menu_item(char *itext, char *kc){
    int text_len = (unsigned)strlen(itext);
    int kc_len = (unsigned)strlen(kc);
    text = new char[text_len]; key_combination = new char[kc_len];
    strcpy(this->text,itext); strcpy(this->key_combination, kc);
}
menu_item::menu_item(menu_item &other_menu_item){
    //key combination
    int size_kc = (unsigned)strlen(other_menu_item.key_combination);
    key_combination = new char[size_kc];
    strcpy(key_combination,other_menu_item.key_combination);
    //text
    int size_text =strlen(other_menu_item.text);
    text = new char[size_text];
    strcpy(text,other_menu_item.text);
}
menu_item::~menu_item(){
    delete []text;
    delete []key_combination;
}
void menu_item::render(std::ostream &stream, unsigned int depth){
    std::string tmp0(text);
    std::string tmp1(key_combination);
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    stream <<"Menu Item \""<<tmp0.substr(0,tmp0.find_first_of('\n'))<<"\" \""<<tmp1.substr(0,tmp1.find_first_of('\n'))<<"\""<<std::endl;
}
