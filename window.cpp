/*
Copyright 2013, Zola Mahlaza
http://www.gnu.org/licenses/gpl-2.0.txt

window class
assignment 2 of csc3022
*/
#include "window.h"
#include<cstring>
using namespace mhlzol004;
window::window(char *some_title){
    int len = strlen(some_title);
    title = new char[len];
    strcpy(title, some_title);
}
window::~window(){
    delete [] title;
}
void window::render(std::ostream &stream, unsigned int depth){
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    std::string tmp(title);
    stream <<"Window \""<<tmp.substr(0,tmp.find_first_of('\n'))<<"\""<<std::endl;
    for (std::vector<widget*>::iterator it = children.begin(); it!=children.end(); ++it){
        (*it)->render(std::cout,depth+1);
    }
}
