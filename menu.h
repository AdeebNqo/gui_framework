#ifndef MENU_H
#define MENU_H
#include "container.h"
#include "menu_item.h"
#include "counter.h"
using namespace sjp;
namespace mhlzol004{
class menu: public container, public counter<menu>{
public:
    char *text;
    char *key_combination;
    menu(char *itext, char *ikey_combination);
    menu(menu &other);
    virtual ~menu();
    virtual void render(std::ostream &stream, unsigned int depth);
    virtual void add_child(menu_item *new_item);
};
}
#endif // MENU_H
