#include "menu.h"
#include "menu_item.h"
#include<cstring>
using namespace mhlzol004;
menu::menu(char *itext, char *ikey_combination){
    int text_len = (unsigned)strlen(itext);
    int kc_len = (unsigned)strlen(ikey_combination);
    text = new char[text_len]; key_combination = new char[kc_len];
    strcpy(text, itext); strcpy(key_combination, ikey_combination);
}
menu::~menu(){
    delete []text;
    delete []key_combination;
}
void menu::add_child(menu_item* new_item){
    this->children.push_back(new_item);
}
void menu::render(std::ostream &stream, unsigned int depth){
    for (unsigned int i=0;i<depth;++i){stream<<"   ";}
    std::string text_s(text);
    std::string kc_s(key_combination);
    stream<<"Menu \""<<text_s.substr(0,text_s.find_first_of('\n'))<<"\" \""<<kc_s.substr(0,kc_s.find_first_of('\n'))<<"\""<<std::endl;
    for (std::vector<widget*>::iterator it = children.begin(); it!=children.end(); ++it){
        (*it)->render(std::cout,depth+1);
    }
}
