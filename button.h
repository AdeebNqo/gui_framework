#ifndef BUTTON_H
#define BUTTON_H
#include<string>
#include "widget.h"
#include "counter.h"
using namespace sjp;
namespace mhlzol004{
class button:public widget, public counter<button>{
public:
    char *text;
    button(char *some_string);
    button(button &other_button);
    virtual ~button();
    void operator=(button &other_button);
    virtual void render(std::ostream &stream, unsigned int depth);
};
}
#endif // BUTTON_H
