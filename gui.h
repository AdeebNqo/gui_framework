#ifndef GUI_H
#define GUI_H
#include<vector>
#include "widget.h"
#include<iostream>
namespace mhlzol004{
class gui{
public:
    //root widget
    widget *ancestor;
    //depth of inheritance hierarchy
    unsigned int depth;
    //children,grandchildren,etc of root
    std::vector<widget*> children;
    gui();
    gui(gui& some_other_gui);
    virtual ~gui();
    void operator=(gui& some_other_gui);
    //renders the root widget
    void render(std::ostream &stream);
};
}
#endif // GUI_H
